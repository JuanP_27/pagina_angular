describe('ventana principal', () => {
    it('tiene el español por defecto', () => {
        cy.visit('http://localhost:4200');
        cy.get('a').should('contain', 'Destinos Para Visitar');
        cy.get('h2 b').should('contain', 'Hola es');
    });
});